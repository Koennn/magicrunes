package me.koenn.mr.listeners;

import me.koenn.mr.MagicRunes;
import me.koenn.mr.runes.MagicRune;
import me.koenn.mr.runes.effect.RuneEffect;
import me.koenn.mr.util.CraftUtil;
import me.koenn.mr.util.Recipes;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;

public class CraftListener implements Listener {

    /**
     * Listener for the PrepareItemCraftEvent.
     *
     * @param event PrepareItemCraftEvent instance
     */
    @EventHandler
    public void onPrepareItemCraft(PrepareItemCraftEvent event) {
        //Check if the result of the recipe is a correct.
        if (event.getRecipe().getResult().getType().equals(Recipes.getRuneRecipe().getResult().getType())) {

            //Check if the Recipe is the MagicRune Recipe
            if (CraftUtil.isRuneRecipe(event.getInventory())) {

                //Make a new MagicRune object.
                MagicRune rune = new MagicRune();

                //Set the result slot of the inventory to the MagicRune.
                event.getInventory().setResult(rune.getItem());

                //Check if the Recipe is a RuneEffect recipe.
            } else if (CraftUtil.isEffectRecipe(event.getInventory())) {

                //Make the oldRune variable.
                ItemStack oldRune = new ItemStack(Material.STONE);
                for (ItemStack item : event.getInventory().getContents()) {
                    if (item.getType().equals(Material.STONE)) {
                        oldRune = item;
                    }
                }

                //Make sure the oldRune has a lore.
                if (!oldRune.getItemMeta().hasLore()) {
                    return;
                }

                //Make the ingredient variable.
                Material ingredient = Material.AIR;
                for (ItemStack item : event.getInventory().getContents()) {
                    if (item != null && !item.getType().equals(Material.STONE) && !item.getType().equals(Material.AIR)) {
                        ingredient = item.getType();
                    }
                }

                //Make the MagicRune object using the lore.
                MagicRune rune = new MagicRune(oldRune.getItemMeta().getLore());

                //Add the RuneEffect using the ingredient.
                RuneEffect effect = MagicRunes.getEffectRegistry().getEffect(ingredient);
                if (!rune.hasEffect(effect)) {
                    rune.addEffect(effect);
                }

                //Set the result slot of the inventory to the MagicRune.
                event.getInventory().setResult(rune.getItem());
            }
        }
    }
}
