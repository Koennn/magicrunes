package me.koenn.mr.listeners;

import me.koenn.mr.player.MagicPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {

    /**
     * Listener for the PlayerJoinEvent.
     *
     * @param event PlayerJoinEvent instance.
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        //Make local variables.
        Player player = event.getPlayer();

        //Check if the player doesn't exist already.
        for (MagicPlayer magicPlayer : MagicPlayer.players) {
            if (player.getUniqueId().equals(magicPlayer.getUuid())) {
                return;
            }
        }

        //Make the MagicPlayer object and register it.
        MagicPlayer magicPlayer = new MagicPlayer(player);
        MagicPlayer.players.add(magicPlayer);
    }
}
