package me.koenn.mr.listeners;

import me.koenn.mr.MagicRunes;
import me.koenn.mr.player.MagicPlayer;
import org.bukkit.Bukkit;

public class EquipListener {

    /**
     * Start the EquipListener to call the RuneEffect onEquip method for every Player every 10 ticks.
     */
    public static void startListener() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicRunes.getPlugin(), () -> MagicPlayer.players.stream().filter(player -> player.getOnlinePlayer() != null).forEach(player -> {

        }), 0, 10);
    }
}
