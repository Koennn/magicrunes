package me.koenn.mr.player;

import me.koenn.mr.runes.MagicRune;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class MagicPlayer {

    /**
     * Static ArrayList of all registered players.
     */
    public static ArrayList<MagicPlayer> players = new ArrayList<>();

    /**
     * Properties of the MagicPlayer object.
     */
    private UUID uuid;
    private String name;
    private MagicRune equippedRune;

    /**
     * Custom MagicPlayer object constructor.
     *
     * @param player Bukkit Player object.
     */
    public MagicPlayer(Player player) {
        this.uuid = player.getUniqueId();
        this.name = player.getName();
    }

    /**
     * Get the MagicPlayer object using the Player name.
     *
     * @param name String name
     * @return MagicPlayer object
     */
    public static MagicPlayer getPlayer(String name) {
        for (MagicPlayer player : players) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        throw new IllegalArgumentException("Player does not exist");
    }

    /**
     * Get the UUID of the Player.
     *
     * @return UUID object
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * Get the name of the Player.
     *
     * @return String playerName
     */
    public String getName() {
        return name;
    }

    /**
     * Get all currently equipped MagicRunes of the Player.
     *
     * @return List<MagicRune> equippedRunes
     */
    public MagicRune getEquippedRune() {
        return equippedRune;
    }

    /**
     * Get the equipped MagicRune of the MagicPlayer.
     *
     * @param rune MagicRune object
     */
    public void setEquippedRune(MagicRune rune) {
        this.equippedRune = rune;
    }

    /**
     * Get the Player object from the MagicPlayer.
     *
     * @return Player object.
     */
    public Player getOnlinePlayer() {
        try {
            Player player = Bukkit.getPlayer(this.uuid);
            if (player.isOnline()) {
                return player;
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }

    /**
     * Get the OfflinePlayer object from the MagicPlayer.
     *
     * @return OfflinePlayer object
     */
    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(this.uuid);
    }
}
