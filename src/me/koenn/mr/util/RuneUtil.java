package me.koenn.mr.util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class RuneUtil {

    /**
     * Get the lore with the No Effects text.
     *
     * @return List<String> lore
     */
    public static List<String> getNoEffectsLore() {
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.BLUE + "No effects");
        return lore;
    }

    public static boolean isRune(ItemStack itemStack) {
        return itemStack.getType().equals(Material.STONE) && itemStack.getItemMeta().hasLore() && itemStack.getItemMeta().hasEnchants();
    }
}
