package me.koenn.mr.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Recipes {

    /**
     * ArrayList of all RuneEffect ShapelessRecipes.
     */
    private static ArrayList<ShapelessRecipe> effectRecipes = new ArrayList<>();

    /**
     * Get the ArrayList of all RuneEffect ShapelessRecipes.
     *
     * @return ArrayList<ShapelessRecipe> effectRecipes
     */
    public static ArrayList<ShapelessRecipe> getEffectRecipes() {
        return effectRecipes;
    }

    /**
     * Add a ShapelessRecipe for a RuneEffect with a certain ingredient.
     *
     * @param ingredient Material ingredient
     */
    public static void addEffectRecipe(Material ingredient) {
        ItemStack result = new ItemStack(Material.STONE);
        ItemMeta meta = result.getItemMeta();
        meta.setDisplayName("Effect Recipe");
        result.setItemMeta(meta);
        effectRecipes.add(new ShapelessRecipe(result).addIngredient(ingredient).addIngredient(Material.STONE));
    }

    /**
     * Get the ShapedRecipe of the MagicRune.
     *
     * @return ShapedRecipe runeRecipe
     */
    public static ShapedRecipe getRuneRecipe() {
        ItemStack result = new ItemStack(Material.STONE);
        ItemMeta meta = result.getItemMeta();
        meta.setDisplayName("Rune Recipe");
        result.setItemMeta(meta);
        ShapedRecipe runeRecipe = new ShapedRecipe(result);
        runeRecipe.shape("nqn", "gsr", "nbn");
        runeRecipe.setIngredient('n', Material.GOLD_NUGGET);
        runeRecipe.setIngredient('s', Material.STONE);
        runeRecipe.setIngredient('q', Material.QUARTZ);
        runeRecipe.setIngredient('g', Material.GLOWSTONE_DUST);
        runeRecipe.setIngredient('r', Material.REDSTONE);
        runeRecipe.setIngredient('b', Material.BLAZE_POWDER);
        return runeRecipe;
    }
}
