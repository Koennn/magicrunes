package me.koenn.mr.util;

import org.bukkit.inventory.CraftingInventory;

public class CraftUtil {

    public static boolean isRuneRecipe(CraftingInventory inventory) {
        return inventory.getResult().getItemMeta().hasDisplayName() && inventory.getResult().getItemMeta().getDisplayName().equals("Rune Recipe");
    }

    public static boolean isEffectRecipe(CraftingInventory inventory) {
        return inventory.getResult().getItemMeta().hasDisplayName() && inventory.getResult().getItemMeta().getDisplayName().equals("Effect Recipe");
    }
}
