package me.koenn.mr.util;

import me.koenn.mr.MagicRunes;
import me.koenn.mr.runes.effect.EffectRegistry;
import me.koenn.mr.runes.effect.EffectType;
import me.koenn.mr.runes.effect.RuneEffect;
import org.apache.commons.lang.NotImplementedException;
import org.bukkit.Material;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class EffectUtil {

    /**
     * Load all RuneEffect objects.
     */
    public static void loadEffects() {
        //Send console message.
        MagicRunes.getPlugin().getLogger().info("Loading all effects...");

        //Get all files in the effects folder.
        File dir = new File(MagicRunes.getPlugin().getDataFolder().getPath() + "\\effect");
        File[] directoryListing = dir.listFiles();

        //Loop through all effect files.
        if (directoryListing != null) {
            for (File file : directoryListing) {

                //Make sure the file is a json file.
                if (file.getName().endsWith(".json")) {

                    //Load the RuneEffect from the file.
                    loadEffect(file);
                }
            }
        }
    }

    /**
     * Load a RuneEffect from a JSON file.
     *
     * @param effectFile JSON effect file
     */
    private static void loadEffect(File effectFile) {
        //Parse the effect JSON file.
        Object obj;
        try {
            JSONParser parser = new JSONParser();
            obj = parser.parse(new FileReader(effectFile));
        } catch (IOException | ParseException e) {
            MagicRunes.getPlugin().getLogger().severe("Failed to parse effect " + effectFile.getName());
            return;
        }

        //Make local variables.
        JSONObject effectObject = (JSONObject) obj;
        EffectRegistry registry = MagicRunes.getEffectRegistry();

        try {
            //Make the variables of the effect.
            String name = (String) effectObject.get("name");
            Material ingredient = Material.valueOf(((String) effectObject.get("ingredient")).toUpperCase());
            EffectType type = EffectType.valueOf(((String) effectObject.get("type")).toUpperCase());

            //Check which type of effect it is.
            if (type.equals(EffectType.BUILTIN)) {
                //Get the constructor arguments (if there are any)
                Object[] args;
                try {
                    args = ((String) effectObject.get("args")).split(", ");
                } catch (Exception ex) {
                    args = null;
                }

                //Register the effect.
                registry.registerEffect(newInstance(name, args), ingredient);
            } else {
                //TODO: Add CUSTOM effect type.
                throw new NotImplementedException();
            }
        } catch (Exception ex) {
            MagicRunes.getPlugin().getLogger().severe("Failed to read effect " + effectFile.getName());
        }
    }

    /**
     * Make a new instance of a RuneEffect child class.
     *
     * @param className Name of the class
     * @param args      Constructor arguments
     * @return RuneEffect object
     * @throws Exception
     */
    public static RuneEffect newInstance(String className, @Nullable Object[] args) throws Exception {
        //Get the class using the className.
        Class<?> clazz = Class.forName(className);

        //If there are no arguments, simply call the constructor.
        if (args == null || args.length == 0) {
            return (RuneEffect) clazz.newInstance();
        }

        //Call the constructor with the arguments.
        List<Class<?>> argTypes = new ArrayList<>();
        for (Object object : args) {
            argTypes.add(object.getClass());
        }
        Constructor<?> explicitConstructor = clazz.getConstructor(argTypes.toArray(new Class[argTypes.size()]));
        return (RuneEffect) explicitConstructor.newInstance(args);
    }
}
