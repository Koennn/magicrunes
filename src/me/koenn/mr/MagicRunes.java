package me.koenn.mr;

import me.koenn.mr.listeners.CraftListener;
import me.koenn.mr.listeners.EquipListener;
import me.koenn.mr.listeners.PlayerListener;
import me.koenn.mr.runes.effect.EffectRegistry;
import me.koenn.mr.util.EffectUtil;
import me.koenn.mr.util.Recipes;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * MagicRunes plugin.
 *
 * @author Koenn
 */
public class MagicRunes extends JavaPlugin {

    /**
     * Static EffectRegistry object.
     */
    private static EffectRegistry effectRegistry;
    /**
     * Static current Plugin instance.
     */
    private static Plugin instance;

    /**
     * Get the current EffectRegistry object.
     *
     * @return EffectRegistry object
     */
    public static EffectRegistry getEffectRegistry() {
        return effectRegistry;
    }

    /**
     * Get the current Plugin instance.
     *
     * @return JavaPlugin instance.
     */
    public static Plugin getPlugin() {
        return instance;
    }

    /**
     * Simple logger method.
     *
     * @param msg message to log.
     */
    public void log(String msg) {
        getLogger().info(msg);
    }

    @Override
    public void onEnable() {
        //Set the current Plugin instance.
        instance = this;

        //Send credit message.
        log("All credits for this plugin go to Koenn");

        effectRegistry = new EffectRegistry();

        if (!this.getDataFolder().exists()) {
            this.getDataFolder().mkdir();
        }

        if (!new File(this.getDataFolder().getPath() + "\\effect").exists()) {
            new File(this.getDataFolder().getPath() + "\\effect").mkdir();
        }

        //Register Bukkit event Listener.
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new CraftListener(), this);

        //Start the EquipListener.
        EquipListener.startListener();

        //Load all RuneEffects.
        EffectUtil.loadEffects();

        //Register all the recipes.
        this.registerRecipes();
    }

    /**
     * Register the recipes from the Recipes class.
     */
    private void registerRecipes() {
        //Register the rune recipe.
        this.getServer().addRecipe(Recipes.getRuneRecipe());

        //Register all the effect recipes.
        Recipes.getEffectRecipes().forEach(recipe -> this.getServer().addRecipe(recipe));
    }

    @Override
    public void onDisable() {
        log("All credits for this plugin go to Koenn");
    }
}
