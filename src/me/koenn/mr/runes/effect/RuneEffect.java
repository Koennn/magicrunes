package me.koenn.mr.runes.effect;

import me.koenn.mr.player.MagicPlayer;
import org.bukkit.Material;

public interface RuneEffect {

    /**
     * Get the name of the RuneEffect.
     * Used to register the effect.
     *
     * @return String name
     */
    String getName();

    /**
     * Gets called every 10 ticks for every player who has the MagicRune equipped.
     *
     * @param player Player object
     */
    void onUpdate(MagicPlayer player);

    /**
     * Gets called whenever a Player equipped a MagicRune.
     *
     * @param player Player object
     */
    void onEquip(MagicPlayer player);

    /**
     * Set the crafting ingredient needed for the RuneEffect.
     *
     * @return Material ingredient.
     */
    Material getIngredient();

    /**
     * Get the crafting ingredient needed for the RuneEffect.
     *
     * @param ingredient Material ingredient.
     */
    void setIngredient(Material ingredient);
}
