package me.koenn.mr.runes.effect.effects;

import me.koenn.mr.player.MagicPlayer;
import me.koenn.mr.runes.effect.RuneEffect;
import org.bukkit.Material;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@SuppressWarnings("unused")
public class RunePotionEffect implements RuneEffect {

    private String name;
    private Material ingredient;
    private PotionEffectType potionEffectType;

    public RunePotionEffect(String name, String potionEffectType) {
        this.name = name;
        this.potionEffectType = PotionEffectType.getByName(potionEffectType);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void onUpdate(MagicPlayer player) {
        player.getOnlinePlayer().addPotionEffect(new PotionEffect(potionEffectType, 100, 1, true, false));
    }

    @Override
    public void onEquip(MagicPlayer player) {

    }

    @Override
    public Material getIngredient() {
        return this.ingredient;
    }

    @Override
    public void setIngredient(Material ingredient) {
        this.ingredient = ingredient;
    }
}
