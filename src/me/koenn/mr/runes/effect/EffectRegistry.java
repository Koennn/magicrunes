package me.koenn.mr.runes.effect;

import me.koenn.mr.MagicRunes;
import me.koenn.mr.util.Recipes;
import org.bukkit.ChatColor;
import org.bukkit.Material;

import java.util.ArrayList;

public class EffectRegistry {

    /**
     * List of all registered RuneEffects
     */
    private ArrayList<RuneEffect> effects = new ArrayList<>();

    /**
     * Get a RuneEffect using his name.
     *
     * @param name Name of the RuneEffect
     * @return RuneEffect object
     */
    public RuneEffect getEffect(String name) {
        for (RuneEffect effect : effects) {
            if (effect.getName().equals(ChatColor.stripColor(name))) {
                return effect;
            }
        }
        throw new IllegalArgumentException("Effect '" + name + "' doesn't exist");
    }

    /**
     * Get a RuneEffect using his crafting ingredient.
     *
     * @param ingredient Material ingredient
     * @return RuneEffect object
     */
    public RuneEffect getEffect(Material ingredient) {
        for (RuneEffect effect : effects) {
            if (effect.getIngredient().equals(ingredient)) {
                return effect;
            }
        }
        throw new IllegalArgumentException("Effect with ingredient '" + ingredient + "' doesn't exist");
    }

    /**
     * Register a new RuneEffect and his crafting ingredient.
     *
     * @param effect     RuneEffect object to register
     * @param ingredient Material ingredient
     */
    public void registerEffect(RuneEffect effect, Material ingredient) {
        effects.add(effect);
        effect.setIngredient(ingredient);
        Recipes.addEffectRecipe(ingredient);
        MagicRunes.getPlugin().getLogger().info("Loaded effect " + effect.getName());
    }
}
