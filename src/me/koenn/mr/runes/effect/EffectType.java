package me.koenn.mr.runes.effect;

public enum EffectType {

    BUILTIN, CUSTOM
}
