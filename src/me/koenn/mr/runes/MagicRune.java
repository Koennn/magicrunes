package me.koenn.mr.runes;

import me.koenn.mr.MagicRunes;
import me.koenn.mr.runes.effect.EffectRegistry;
import me.koenn.mr.runes.effect.RuneEffect;
import me.koenn.mr.util.RuneUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MagicRune {

    /**
     * ArrayList of all RuneEffects
     */
    private ArrayList<RuneEffect> runeEffects;

    /**
     * Basic constructor for a MagicRune with no effects.
     */
    public MagicRune() {
        this.runeEffects = new ArrayList<>();
    }

    /**
     * Constructor for a MagicRune with pre-defined effects.
     *
     * @param effects List of effects
     */
    public MagicRune(List<String> effects) {
        this.runeEffects = new ArrayList<>();
        EffectRegistry registry = MagicRunes.getEffectRegistry();
        this.runeEffects.addAll(effects.stream().filter(effect -> !effect.contains("No effect")).filter(effect -> registry.getEffect(effect) != null).map(registry::getEffect).collect(Collectors.toList()));
    }

    /**
     * Get the ItemStack of the MagicRune.
     * Automaticly adds the No Effects if there are no effects.
     *
     * @return ItemStack rune
     */
    public ItemStack getItem() {
        ItemStack rune = new ItemStack(Material.STONE);
        ItemMeta meta = rune.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addEnchant(Enchantment.DURABILITY, 1, true);
        meta.setDisplayName(ChatColor.WHITE + "Magic Rune");
        meta.setLore(runeEffects.stream().map(effect -> ChatColor.BLUE + effect.getName()).collect(Collectors.toCollection(ArrayList::new)));
        if (runeEffects.isEmpty()) {
            meta.setLore(RuneUtil.getNoEffectsLore());
        }
        rune.setItemMeta(meta);
        return rune;
    }

    /**
     * Add a RuneEffect to the MagicRune.
     *
     * @param effect RuneEffect object
     */
    public void addEffect(RuneEffect effect) {
        this.runeEffects.add(effect);
    }

    /**
     * Check if the rune has a certain RuneEffect.
     *
     * @param effect RuneEffect object
     * @return boolean hasEffect
     */
    public boolean hasEffect(RuneEffect effect) {
        return this.runeEffects.contains(effect);
    }

    /**
     * Get all the RuneEffects on this rune.
     *
     * @return ArrayList<RuneEffect> effects
     */
    public ArrayList<RuneEffect> getRuneEffects() {
        return runeEffects;
    }
}
